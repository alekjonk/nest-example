import { ApiProperty } from '@nestjs/swagger';
import { Movie } from '../movie.entity';

export class MovieResponse
  implements Partial<Record<keyof Movie, any>>{
  @ApiProperty({ type: Movie, example: 1, description: 'The id of the movie' })
  id: number;

  @ApiProperty({ type: Movie, example: 'Inception', description: 'The title of the movie' })
  title: string;

  @ApiProperty({ type: Movie, example: 2010, description: 'The release year of the movie' })
  year: number;

  @ApiProperty({ type: Movie, example: 'Sci-Fi', description: 'The genre of the movie' })
  genre: string;

  constructor(movie: Movie) {
    this.id = movie.id;
    this.title = movie.title;
    this.year = movie.year;
    this.genre = movie.genre;
  }
}