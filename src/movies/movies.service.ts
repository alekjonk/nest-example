import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Movie } from './movie.entity';

@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(Movie)
    private readonly movieRepository: Repository<Movie>,
  ) {}

  async findAll(): Promise<Movie[]> {
    return this.movieRepository.find();
  }

  async findOne(id: number): Promise<Movie> {
    const movie = await this.movieRepository.findOneBy({ id });

    if (!movie) {
      throw new NotFoundException(`Movie with ID ${id} not found`);
    }

    return movie;
  }

  async findByTitle(title: string): Promise<Movie> {
    const movie = await this.movieRepository.findOneBy({title});

    if(!movie){ 
      throw new NotFoundException(`Movie with title ${title} not found`);
    }

    return movie;
  }

  async create(movie: Movie): Promise<Movie> {
    return this.movieRepository.save(movie);
  }

  async update(id: number, movie: Movie): Promise<Movie> {
    await this.findOne(id); 

    await this.movieRepository.update(id, movie);
    return this.findOne(id);
  }

  async remove(id: number): Promise<void> {
    const movie = await this.findOne(id); 
    await this.movieRepository.remove(movie);
  }
}
