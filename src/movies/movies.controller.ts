import { Controller, Get, Post, Body, Param, Put, Delete, Query } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiProperty } from '@nestjs/swagger';
import { MoviesService } from './movies.service';
import { Movie } from './movie.entity';
import { MovieResponse } from './dto/movie-response.dto';

  

@Controller('movies')
@ApiTags('movies') 
export class MoviesController {
  constructor(private readonly moviesService: MoviesService) {}

  @Get()
  @ApiOperation({ summary: 'Get all movies', operationId: 'findAll' })
  @ApiResponse({ status: 200, description: 'Return all movies' })
  async findAll(): Promise<{rows: MovieResponse[]}> {
    const movies =  await this.moviesService.findAll();
    const movieList = movies.map((movie: Movie) => new MovieResponse(movie));
    return { rows: movieList };  }

  @Get('/title/:title')
  @ApiOperation({ summary: 'Get a movie by title', operationId: 'findByTitle' })
  @ApiResponse({ status: 200, description: 'Return a movie by title' })
  @ApiResponse({ status: 404, description: 'Movie not found' })
  findByTitle(@Param('title') title: string): Promise<MovieResponse> {
    return this.moviesService.findByTitle(title);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get a movie by ID', operationId: 'findOne' })
  @ApiResponse({ status: 200, description: 'Return a movie by ID' })
  @ApiResponse({ status: 404, description: 'Movie not found' })
  findOne(@Param('id') id: number): Promise<MovieResponse> {
    return this.moviesService.findOne(+id);
  }

  @Post()
  @ApiOperation({ summary: 'Create a new movie', operationId: 'create' })
  @ApiResponse({ status: 201, description: 'The movie has been successfully created' })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  create(@Body() movie: Movie): Promise<MovieResponse> {
    return this.moviesService.create(movie);
  }

  @Put(':id')
  @ApiOperation({ summary: 'Update a movie by ID', operationId: 'update' })
  @ApiResponse({ status: 200, description: 'The movie has been successfully updated' })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 404, description: 'Movie not found' })
  update(@Param('id') id: number, @Body() movie: Movie): Promise<MovieResponse> {
    
    return this.moviesService.update(+id, movie);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a movie by ID', operationId: 'remove' })
  @ApiResponse({ status: 200, description: 'The movie has been successfully deleted' })
  @ApiResponse({ status: 404, description: 'Movie not found' })
  remove(@Param('id') id: number): Promise<void> {
    return this.moviesService.remove(+id);
  }
}
